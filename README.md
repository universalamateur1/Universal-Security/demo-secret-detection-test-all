# Flask App explictly Vulnerbale with Secrets

For GitLab Team Memebers to demo Secret Detection to our customers.

## Runbook

1. Fork this project into an Ultimate group for yourself.
2. Remove the [Forking relationship](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#unlink-a-fork).
3. Activate Secret Detection via the [UI through an automatically configured merge request](https://docs.gitlab.com/ee/user/application_security/secret_detection/#use-an-automatically-configured-merge-request).
4. Show after the Pipeline has run the 8 found secrets in the security Tab of the Pipeline and in the Vulnerability report.
5. In the Pipeline Secuirty Tab select the Secrets found in the Readme.md and dismiss those with `Won't fix, Accpet risk`.
6. Switch to the vulnerability report and show one vulnerability in the popup modal.
7. Create an issue out of this.
8. Go back to the vulnerability report and select all findings.
9. Set their status to confirmed
10. Explain that there are some hidden Secrets, which were deleted with newer commits.
11. Create A Merge Request on the branch `adding-the-gitlab-gitlab-ci-yml-file` to `main` and merge, so that the `.gitlab-ci.yml` is then existing in your main branch.
12. The Merge executes a pipeline on the default brnach with the new settings (Git History) and you can show off that 3 new Secrets were found.
13. Create an issue out of one of those findings to jump to that Secret.
14. Show in the file [`app.py` in line 73](https://gitlab.com/universalamateur1/Universal-Security/demo-secret-detection/-/blob/main/app.py#L73) the `headers = {"Authorization": "Bearer MYREALLYLONGTOKENIGOT"}` and with it, that this Api key has not been detected.
15. Open the in the branch `adding-customised-ruleset-for-secret-detection` the `.gitlab` folder with the extension forward file `.gitlab/secret-detection-ruleset.toml` and the extended config in `.gitlab/extended-gitleaks-config.toml` (Ued concept are Allowlist and extend, used RegEx [found here](https://regex101.com/r/PGscpO/1)).
16. Create a Merge Request `adding-customised-ruleset-for-secret-detection` to `main` so that the `.gitlab` folder is present in your main Branch.
17. The Merge executes a pipeline on the default branch with the new findings, which you can show in the Security Tab or Vulnerability report.
18. _Optional:_ **Security Policies:**
19. _Optional:_ Go to in your repo to `security/policies`.
20. _Optional:_ Click `New Policy`.
21. _Optional:_ In `Scan result policy` click `Select Policy`.
22. _Optional:_ Give Name, for example `No Secrets Here`.
23. _Optional:_ Select: When `Security Scanners` `Secret Detection` runs against the `All protected branches` and find(s) `Any` vulnerabilities that match all of the following criteria:
24. _Optional:_ Click Add new criteria -> New Status.
25. _Optional:_ Status is: `New` `All vulnerability states`.
26. _Optional:_ Got to the Actions part.
27. _Optional:_ Require 1 approval from `Groups`: `Enter a Group Name` for example `timtams`.
28. _Optional:_ Click `Create with a Merge Request`.
29. _Optional:_ `Merge` the Merge request.
30. _Optional:_ Show off the newly create [Security policy project](https://docs.gitlab.com/ee/user/application_security/policies/#security-policy-project).
31. _Optional:_ Show the `.gitlab/security-policies/policy.yml` in that project as rules as Code.
32. _Optional:_ Switch back to your original project.
33. Show the addition of a secret in a Merge request.
34. Open an issue in your Project, call it `Adding a Secret`.
35. Add in the description this GitLab PAT to show of the warning dialogue: `glpat-JUST20LETTERSANDNUMB`.
36. Open a Merge Request out of this issue.
37. Open the WebIDE in this Merge Request.
38. Open any file of the repo in the WebIDE.
39. Add this content to the file: `Demo Token: glpat-JUST20LETTERSANDNUMB`.
40. Push the change via the WebIDE to your repo.
41. Open the Merge Request again and show the pipeline plus after it has run the security finding.
42. _Optional:_ If you have security Policies setup, show that it needs aproval.
43. Open again the WebIDE with the file where you had added the Secret.
44. Show how you can [Ignore Secrets](https://docs.gitlab.com/ee/user/application_security/secret_detection/#ignore-secrets).
45. Add behind the Secret in your code: `#gitleaks:allow`.
46. Commit the Code Change.
47. Show that the Secret is not found anymore and you can Merge.
48. **Activate Push Rules:**
49. Go in the NavBar to `Settings`, there `Repository Settings` and Expand `Push rules`.
50. Activate `Prevent pushing secret files` and click `Save Push Rules`.
51. Back in the repository open the WebIDE.
52. Create a new file with the name 'id_rsa'.
53. Copy in the content from below [Demo RSA private Key].
54. Commit and push the changes to the main brnach.
55. Show the Error message which comes up.

### Demo RSA private Key

```bash
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAwFR40KI1SzBR0njomkDDEeS0c4r6+7HoUlzqE24hcnP0eJW1
xa2i1dBMkhm56bhWIIq6w6F9Fg8bd5Tsr72OYsiDpoeJ6KV3RVFDv6Ighja+/WdP
4DroQfq0YVo2XoBSPb5uhv46/pNUa5Y+rwVh+SPQlH8sxE7LZODoy8FD2+GskSUs
jk9Y3Cca5/cv7wafh/i3/De5tE5DG1pLlNwbdAG3xCgcgS2806XCMgA5mXt9HXqn
vyuoyyG2gW6sdHb1pwf8rlgoRLbPqwbLpOi245f5XqLmmwBu4Nxc3scseDXfuHDp
Nky1cOzIBePVxktM2DlAAPQ+7Em23TJr0pmrQQIDAQABAoIBAQCnsX9dufDpzAmr
kAyPYmQzV8wW6lkH2AkOt0DJDD9RgdToxvAkmc7eyq3YvWGibT17RjqtlEJyV13F
mC3+1TIu41IWgxs1pAAoikCd+AiPvXAtlkTI59PWo3dfYr8BCrWqbD4GqehaS69R
10B0bicMibO1pmUsDN++53NTJQG71rZp8o3b39Pse3ON/fQLpflPdaBOGkcyXVu5
UB4UDpO8PHtGU3IsxdpM1AVLpari/cYTksgTVczYtYDcTT4Ud2kuB4Q9OmwDTT2K
uecn7S4FR1FS58ILHhC67gWO/A6DnXTUqwiD8/pDY8QJcRQviWoEBQyhMycZSH+O
enzJtCPZAoGBAPPgVlFRMM99r2zSqT0btIG986Pyh3q8dz8ZvfdwUSIbRgqPAG47
ZukRqIVl2d7S9yVtRnFnDteNokG4d5Mk1a3syn87iFKE6wBpr4htWSlOlntVakou
iQq1ngFcA6ABAwtv2eEa1BPaWMuW7q3yWdOYrC36vX1A03llyp4xFNzfAoGBAMnk
IuT1ZRJZpAqd9SveEqW+aYpUrFetYz3JhVuNT49vJoBiq1RrWvgfuEC7gayEDDfb
Gep11XnXPkXspN6415kdarOgiE9CQlADNG9fk0D61O5ONZZTrqGWEBythfoV2xSk
xb6YDPuxs0S6MylQAc9ZRVUpVGLnytHsKjAMVlvfAoGASxFZ4Iv6V1QbxIaPu5Sk
mm8q6ONFmp0ao5y74cd74eC9TZC5FDVKtyFNW0p/ptwPYUDitxN++RDKyioK/IsR
DwldR46+po/temINuxPVpyZeobYoEo+CdX50FX0KTJ0jH8kdKvJEJ5xFSt25uGdq
CPzsuvZ8j2p97ddMaCc5gccCgYAdUI7wh+FBJNr43661y+0RO/C/MURFBtweIKDI
hmBDB3Sjt7AA9gWjeZebbp6JmjLb+Wht7uYsZuCX7qCR5m0Hwom3w1uHhqtySsTW
Vx5elQ1N/PUy+rukotF8GIYXpgzFlpdP8WwRL+BD3nWHTiK1JNU4ZGPoaJe+m3gU
ufXgKQKBgFdbqJfnSrXyrJcDH9nzMwnR6wyFc0RnILie1lrcQZiru9s/Tlv71GJ/
nMEybyOailEeSlBKYy04uVJGPVO4bnzIiGmJdgZlxxjEwHpx/6bkBnh4YVbRAHgc
al8MbhHvfVaRRdRW8eRVuoeHfPge8fRr7UtloYbOEpZh9nTxMUHj
-----END RSA PRIVATE KEY-----
```

## Resources

* [Docu Push rules](https://docs.gitlab.com/ee/user/project/repository/push_rules.html#prevent-pushing-secrets-to-the-repository)
* [Files deny list for push rules](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/lib/gitlab/checks/files_denylist.yml)
* [Secuirty Policies in docs](https://docs.gitlab.com/ee/user/application_security/policies/#security-policy-project)
* [Secret Dteection in docs](https://docs.gitlab.com/ee/user/application_security/secret_detection/)
* [Saved regex](https://regex101.com/r/PGscpO/1)

### RegEx Examplesa

```python
regex = '`:\s*['" ](?i)bearer\S* \S+`gm'
headers = {"Authorization": "Bearer MYREALLYLONGTOKENIGOT"}
metaData= {'authorization' : "bearer jdewRVKNL3AQ52AWSZwQ9WddDbOvHtQuW2O",
WWW-Authenticate: Bearer realm="Keksfabrik"
Authorization: Bearer S0VLU0UhIExFQ0tFUiEK
curl -H "Authorization: bearer MYREALLYLONGTOKENIhad" http://www.example.com
```

## _Optional_ Commands for local installation and prep

* Git Clone this repo
* virtual enviroment `python3 -m venv .`
* Install reqs `python3 -m  pip install -r requirements.txt`
* run app `python3 -m flask run`

## _Optional_ Artifial vulns

### Werkzeug

* [werkzeug@0.15.1 vulnerabilities](https://security.snyk.io/package/pip/werkzeug/0.15.1) - version 0.15.1

### Flask

* [flask@0.12.2 vulnerabilities](https://security.snyk.io/package/pip/flask/0.12.2) - 0.12.2

### Image

* [python:3.8-slim-buster](https://hub.docker.com/layers/library/python/3.8-slim-buster/images/sha256-4bc25e1810c14af78ca0235ff6f63740113540fd49f8dc5bc6c7b456572b5806?context=explore)

## Sevcrets Included

AWS Key: WasHere

GitLab Token: WasHere

GitHub Token: WasHere
